
describe("tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));//comando abri URL

    it("fills all the imput fields", () => {
        const firstName = "Tiago";//Criando uma variável
        const lastName = "Moura";
        
        cy.get("#first-name").type(firstName);//cy.get = identificar ID do campo - type para preencher campo
        cy.get("#last-name").type(lastName);//lastName passando valor da variável
        cy.get("#email").type("tbp.moura@gmail.com");
        cy.get("#requests").type("Não fumante");
        cy.get("#signature").type(`${firstName} ${lastName}`);//preenchendo campo concatenando duas variáveis, obs. usar crase
    });

    it("Select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });
    
    it("Select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });
    
    it("select 'social midia' chebox", () => {
        cy.get("#social-media").check();
    });

    it("selects 'friend' and 'publication', then uncheck 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("alerts on invalid email", () => {
        cy.get("#email")
          .as("email")
          .type("email-errado.com.br");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
          .clear()
          .type("tbp.moura@gmail.com");

        cy.get("#email.invalid").should("not.exist");  
    });

    it("fills and reset the form", () => {
        const firstName = "Tiago";//Criando uma variável
        const lastName = "Moura";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);//cy.get = identificar ID do campo - type para preencher campo
        cy.get("#last-name").type(lastName);//lastName passando valor da variável
        cy.get("#email").type("tbp.moura@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("Não fumante");
        
        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get ("#signature").type(fullName);
        
        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("fills mandatory fields using support comand", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@exemple.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

          cy.get("#agree").uncheck();

          cy.get("@submitButton").should("be.disabled");
    
    });
});